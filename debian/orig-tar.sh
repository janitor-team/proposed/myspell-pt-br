#!/bin/sh

uver=$2
file=$3
pkg=myspell-pt-br
dir=$pkg-$uver

mkdir $dir
cd $dir
unzip -x ../$file
cd ..
tar cfz ../${pkg}_$uver.orig.tar.gz $dir
rm -rf $dir
